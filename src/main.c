#include <stdio.h>
#include <string.h>
#include "funciones.h"
#include "estructuras.h"
#include <stdlib.h>
#include <math.h>


struct rolTDA rol;
struct rolTDA *ptr=&rol;


int main(){
	 //instanciar estructura rolTDA
	ptr->inicio.siguiente = malloc(20*sizeof(struct empleadoTDA));
	int numEmpleados=0;
	char* nombre_Empleado = "Rol de Empresa";
	rol.nombre=nombre_Empleado;
	rol.num_empleados=20;
	char opcion[100]="0";
	while((strcmp(opcion,"6\n")!=0)&&(strcmp(opcion,"-1\n")!=0)){//while del programa
		char *input="";
		printMain();
		printf("Ingrese una de las opciones:\n");
		fgets(opcion, 100, stdin);
		if((strcmp(opcion,"6\n")==0)||(strcmp(opcion,"-1\n")==0)){
			printf("Ha salido del programa Rol de Empresa\n");
		}else if(strcmp(opcion,"1\n")==0){//opcion numero 1
			printf("Opcion: 1\n");
			char *input1;
			while(strcmp(input,"-1\n")!=0){
				//printf("Ingresa la informacion del nuevo Empleado\n");
				printf("Datos: ");
				input1=(char*)malloc(sizeof(char*)*100);
				fgets(input1,100,stdin);
				input=input1;
				int ver = verificador_empleado(input1);
				if(ver==0){
					printf("\nel modelo debe ser:\n");
					printf("nombre,apellido,fecha_nac(year-month-day)\n\n");				
				}else if(ver==1){
					//printf("\n");
					
					char *tokens = (char*)malloc(sizeof(char*));
					int cont;
					if (!(ptr->inicio.nombre)){
						numEmpleados=numEmpleados+1;
						tokens = strtok(input1,",");
						cont=0;
						while(tokens!=NULL){
							if(cont==0){
								ptr->inicio.nombre=tokens;
							}else if(cont==1){
								ptr->inicio.apellido=tokens;
							}else if(cont==2){
								ptr->inicio.fecha_nac=tokens;
							}cont = cont+1;
							tokens = strtok(NULL,",");
						}
					}else{	
						struct empleadoTDA *emplp=malloc(sizeof(struct empleadoTDA));
						tokens = strtok(input,",");
						cont=0;
						while(tokens!=NULL){
							if(cont==0){
								emplp->nombre=tokens;
							}else if(cont==1){
								emplp->apellido=tokens;
							}else if(cont==2){
								emplp->fecha_nac=tokens;
							}cont = cont+1;
							tokens = strtok(NULL,",");
						}
						ptr->inicio.siguiente[numEmpleados-1]=*emplp;
						if(numEmpleados!=1){
							ptr->inicio.siguiente[numEmpleados-2].siguiente=&ptr->inicio.siguiente[numEmpleados-1];
						}
						numEmpleados=numEmpleados+1;
					}
					free(tokens);
				}else{
					//struct empleadoTDA *emplp2=ptr->inicio.siguiente;
					//printf("nombre : %s\n",ptr->inicio.nombre);
					//printf("apellido : %s\n",ptr->inicio.apellido);
					//printf("fecha de nacimiento : %s\n",ptr->inicio. fecha_nac);
					//for(int i = 0;i<numEmpleados-1;i=i+1){	
					//	printf("nombre :%s\n",emplp2->nombre);
					//	printf("apellido:%s\n",emplp2->apellido);
					//	printf("fecha de nacimiento : %s\n",emplp2->fecha_nac);
					//	emplp2=emplp2->siguiente;
					//}
					printf("...\n");	
				}
			}
		}else if(strcmp(opcion,"2\n")==0){//opcion numero 2
			printf("Opcion: 2\n");
			char *input2;
			while(strcmp(input,"-1\n")!=0){
				printf("\n");
				//printf("Asigna un trabajo a un empleado\n");
				printf("Trabajo: ");
				input2=(char*)malloc(sizeof(char*)*100);
				fgets(input2,100,stdin);
				input=input2;
				int ver = verificador_trabajo(input2,ptr->inicio,numEmpleados);
				if(ver==0){
					printf("el modelo debe ser:\n");
					printf("nombre,apellido,sueldo,fecha_inicio,fecha_fin,puesto,departamento\n");	
				}else if(ver==1){
					int i;
					int existe=0;
					char *token=strtok(input2,",");
					int cont;
					char *nombre;
					char* apellido;
					if(ptr->inicio.nombre!=NULL){
						struct empleadoTDA *emplp2=ptr->inicio.siguiente;
						nombre=token;
						token=strtok(NULL,",");
						apellido=token;
						if (strcmp(nombre,ptr->inicio.nombre)==0){	
							if(strcmp(apellido,ptr->inicio.apellido)==0){
								existe=1;
								
							}				
						}
						i=0;
						while((i<numEmpleados-1)&&(existe!=1)){	
							if (strcmp(nombre,emplp2->nombre)==0){	
								if(strcmp(apellido,emplp2->apellido)==0){
									existe=1;
								}				
							}
							emplp2=emplp2->siguiente;
							i=i+1;
						}
					}if(existe==0){
						printf("El empleado no existe");	
					}else{
						//printf("el empleado existe\n");
						//creaando el puestoTDA
						//printf("indice de empl %d\n",i);
						struct puestoTDA *puesto = malloc(sizeof(struct puestoTDA));
						token = strtok(NULL,",");
						float sueldo = atof(token);
						printf("%f\n",sueldo);
						cont=0;
						puesto->sueldo_mensual=sueldo;
						token = strtok(NULL,",");
						while(token!=NULL){
							printf("%s\n",token);
							if(cont==0){
								puesto->fecha_inicio=token;
							}else if(cont==1){
								puesto->fecha_fin=token;
							}else if(cont==2){
								puesto->puesto=token;
							}else if(cont==3){
								puesto->departamento=token;
							}cont = cont+1;
							token = strtok(NULL,",");
						}
						printf("%s %s",puesto->fecha_inicio,puesto->fecha_fin);
						restarFechas(puesto->fecha_inicio,puesto->fecha_fin);
						//printf("%d\n",);
						
						struct empleadoTDA *emplp2=ptr->inicio.siguiente;
							
						if (strcmp(nombre,ptr->inicio.nombre)==0){	
							if(strcmp(apellido,ptr->inicio.apellido)==0){
								emplp2->puesto_actual=*puesto;
								
							}				
						}
						i=0;
						while((i<numEmpleados-1)&&(existe!=1)){	
							if (strcmp(nombre,emplp2->nombre)==0){	
								if(strcmp(apellido,emplp2->apellido)==0){
									emplp2->puesto_actual=*puesto;
								}				
							}
							emplp2=emplp2->siguiente;
							i=i+1;
						}
					
					}
				}else if(ver==3){
					printf("No existen el empleado para asignarle trabajo\n");	
				}else{
					printf("...\n");	
				}
			}
		}else if(strcmp(opcion,"3\n")==0){//opcion numero 3
			printf("Opcion: 2\n");
			while(strcmp(input,"-1\n")!=0){
				printf("eliminar un Empleado\n");
				printf("Trabajo: ");
				fgets(input,50,stdin);
				int ver = verificador_empleado(input);
				if(ver==0){
					printf("el modelo debe ser:");
					printf(" nombre,apellido,fecha_nac(year-month-day)\n");				
				}else if(ver==1){
					printf("\n");
					//empl.nombre=rol.inicio.nombre;
					//while(&empl.nombre!=0){
					
					//	printf("%s",empl.nombre);
					//	empl = *empl.siguiente;
					//}
				}else{
					printf("...\n");	
				}
			}
		}else if(strcmp(opcion,"4\n")==0){//opcion numero 2
			printf("Opcion: 2\n");
			while(strcmp(input,"-1\n")!=0){
				printf("Asignar un trabajo al Empleado\n");
				printf("Trabajo: ");
				fgets(input,50,stdin);
				int ver = verificador_empleado(input);
				if(ver==0){
					printf("el modelo debe ser:");
					printf(" nombre,apellido,fecha_nac(year-month-day)\n");				
				}else if(ver==1){
					printf("\n");
					//empl.nombre=rol.inicio.nombre;
					//while(&empl.nombre!=0){
					
					//	printf("%s",empl.nombre);
					//	empl = *empl.siguiente;
					//}
				}else{
					printf("...\n");	
				}
			}
		}else if(strcmp(opcion,"5\n")==0){//opcion numero 2
			printf("Opcion: 2\n");
			while(strcmp(input,"-1\n")!=0){
				printf("Asignar un trabajo al Empleado\n");
				printf("Trabajo: ");
				fgets(input,50,stdin);
				int ver = verificador_empleado(input);
				if(ver==0){
					printf("el modelo debe ser:");
					printf(" nombre,apellido,fecha_nac(year-month-day)\n");				
				}else if(ver==1){
					printf("\n");
					//empl.nombre=rol.inicio.nombre;
					//while(&empl.nombre!=0){
					
					//	printf("%s",empl.nombre);
					//	empl = *empl.siguiente;
					//}
				}else{
					printf("...\n");	
				}
			}
		}
	}
	return 0;
}


