#include <stdio.h>
#include <string.h>
#include "estructuras.h"
#include <ctype.h>
#include <stdlib.h>

void printMain(){
	printf("Rol de Empresa:\n");
	printf("\t1.Ingresar nuevo empleado\n");
	printf("\t2.Añadir Trabajo\n");
	printf("\t3.Eliminar Empleado\n");
	printf("\t4.Mostrar trabajos anteriores\n");
	printf("\t5.Promedio de sueldos de empleado\n");
	printf("\t6.Salir\n");
}

int verificador_empleado(char *datos){
	int longitud=strlen(datos);
	int cont = 0;
	int correctDate=0;
	if(strcmp(datos,"-1\n")!=0){
		for (int i=0;i<longitud;i=i+1){
			if(cont>=2){
				if((i+1!=longitud)&&(datos[i]=='-')&&(datos[i+1]!='-')&&((correctDate==4)||(correctDate==6))){
					cont++;
				}else if(isdigit(datos[i])){
					correctDate++;
				}
			}
			else if((i+1!=longitud)&&(datos[i]==',')&&(datos[i+1]!=',')){
				cont++;
			}else if(!isalpha(datos[i])){
				cont--;
			}else if(correctDate>8){
				cont--;
			}
		}//printf("contador sep :%d fechas correctas :%d\n",cont,correctDate);
	}else{
		return 2;
	}
	if ((cont==4)&&(correctDate==8)){
		return 1;
	}else{
		return 0;
	}
}
int verificador_trabajo(char *datos,struct empleadoTDA empl,int numEmpleados){
	int longitud=strlen(datos);
	int cont = 0;
	int correctDate=0;
	int numberCount=0;
	int cantpuntos=0;
	if(strcmp(datos,"-1\n")!=0){
		for (int i=0;i<longitud-1;i=i+1){
			if((cont==3)||(cont==4)||(cont==5)||(cont==6)||(cont==7)||(cont==8)){
				if((i+1!=longitud)&&(datos[i]=='-')&&(datos[i+1]!='-')&&((numberCount==4)||(numberCount==6))){
					cont++;
					correctDate++;
					//printf("cont%d date%d \n",cont,correctDate);
				}else if(isdigit(datos[i])){
					numberCount++;
				}else if(numberCount>8){
					cont--;
					//printf("cont - %d \n",cont);
				}
			}
			if((i+1!=longitud)&&(datos[i]==',')&&(datos[i+1]!=',')){
				cont++;
				//printf("cont%d \n",cont);
				numberCount=0;
			}else if(cont==2){
				if((!isdigit(datos[i]))){
					cont--;
					//printf("cont - sueldo %d \n",cont);
				}if((datos[i]=='.')){
					cont++;
					//printf("cont - sueldo %d \n",cantpuntos);
					if(cantpuntos>0){
						cont--;
						//printf("cont - sueldo puntos %d \n",cont);
					}cantpuntos++;
				}
			}else if((cont==0)||(cont==1)||(cont==9)||(cont==10)){
				if(!isalpha(datos[i])){
					cont--;
					//printf("cont - %d \n",cont);
				}
			}
		}
		//printf("contador sep :%d fechas correctas :%d\n",cont,correctDate);
	}else{
		return 2;
	}//printf("\n");	
	if ((cont==10)&&(correctDate==4)){
		return 1;
	}else{
		return 0;
	}
}
int *listanum(char *fi);
int *listanum(char *fi){
	char *tokeni=malloc(sizeof(int)*3);
	tokeni = strtok(fi,"-");
	int i=0;
	static int listi[3];
	//int listf[3];
	while(tokeni!=NULL){
		listi[i]=atoi(tokeni);
		tokeni = strtok(NULL,"-");
		i++;
	}free(tokeni);
	return listi;
}
int restarFechas(char *fi,char* ff){
	int resta=0;
	int *listi=listanum(fi);
	printf("%d\n",listi[0]);
	int *listf=listanum(ff);
	printf("%d\n",listf[0]);
	resta=listi[0]-listf[0];
	printf("%d = %d - %d\n",resta,listi[0],listf[0]);
	//if(valido==1){
	//	return -1;
	//}else{
	//	return resta;
	//}
	return 1;
}
