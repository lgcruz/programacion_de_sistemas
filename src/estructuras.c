struct puestoTDA{
	float sueldo_mensual;
	char *fecha_inicio;
	char *fecha_fin;
	char *puesto;
	int meses;
	char *departamento;
	struct puestoTDA *anterior;
};

struct empleadoTDA{
	char *nombre;
	char *apellido;
	char *fecha_nac;
	struct puestoTDA puesto_actual;
	struct puestoTDA *puesto_ant;
	struct empleadoTDA *siguiente;
};

struct rolTDA{
	char *nombre;
	int num_empleados;
	struct empleadoTDA inicio;
};

