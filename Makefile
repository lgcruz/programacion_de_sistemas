rol : main.o funciones.o estructuras.o
	gcc -g obj/main.o obj/funciones.o obj/estructuras.o -lm -o bin/rol

main.o : src/main.c
	gcc -g -Wall -c -I include/ src/main.c -lm -o obj/main.o

funciones.o : src/funciones.c include/funciones.h
	gcc -g -Wall -c -I include/ src/funciones.c -o obj/funciones.o

estructuras.o : src/estructuras.c include/estructuras.h
	gcc -g -Wall -c -I include/ src/estructuras.c -o obj/estructuras.o
	
